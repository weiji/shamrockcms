package com.shamrock.cms.constant;

public class ArticleConstant {
	public static enum Status{
		/**
		 * 隐藏
		 */
		hidden,
		/**
		 * 公开
		 */
		display
	}
	
	public static enum Check{
		yes,no,init
	}
}
