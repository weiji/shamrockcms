package com.shamrock.cms.exception;

/**
 * @author GunnyZeng
 * 
 */
public class UploadException extends Exception {

	public UploadException(String msg) {
		super(msg);
	}
}
