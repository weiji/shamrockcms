package com.shamrock.cms.exception;

public class PropertyIsNullException extends Exception {
	public PropertyIsNullException(String msg) {
		super(msg);
	}
}
