package com.shamrock.cms.exception;

/**
 * @author GunnyZeng
 * 
 */
public class InstallException extends Exception {

	public InstallException(String msg) {
		super(msg);
	}
}
