	</section>
	</div>

	<!-- js placed at the end of the document so the pages load faster -->
	<script src="${TEMPLATE_MANAGE_PATH}/js/bootstrap.min.js?v=${config_v}"></script>
	<script src="${TEMPLATE_MANAGE_PATH}/js/jquery.form.min.js?v=${config_v}"></script>
	<script src="${TEMPLATE_MANAGE_PATH}/js/bootbox.min.js?v=${config_v}"></script>
	<script src="${TEMPLATE_MANAGE_PATH}/js/jquery.json.js?v=${config_v}"></script>
	<script src="${TEMPLATE_MANAGE_PATH}/assets/fancybox/source/jquery.fancybox.js?v=${config_v}"></script>
	<script src="${TEMPLATE_MANAGE_PATH}/assets/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js?v=${config_v}"></script>
	<script src="${TEMPLATE_MANAGE_PATH}/assets/bootstrap.datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js?v=${config_v}"></script>
	<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/assets/ueditor/ueditor.config.js"></script>
   	<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/assets/ueditor/ueditor.all.min.js"> </script>
   	<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/assets/ueditor/lang/zh-cn/zh-cn.js"></script>
   	<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/assets/uploadify/jquery.uploadify.min.js?v=${config_v}"></script>
	<!--common script for all pages-->
	<script src="${TEMPLATE_MANAGE_PATH}/js/common-scripts.js"></script>
</body>
</html>
