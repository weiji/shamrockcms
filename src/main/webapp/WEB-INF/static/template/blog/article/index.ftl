	<#include "../header.ftl">
	<section class="dexp-section" id="section-main-content">
	<div class="container">
		<div class="row">
			<!-- .region-content-->
<div class="region region-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="tabs"></div>
<div id="block-system-main" class="block block-system">
  <div class="inner"></div>
        
  <div class="content">
    <div class="blog_user" style="width:22.5%;">
	<div class="view view-user-block view-id-user_block view-display-id-entity_view_1 view-dom-id-8bce56599b77f0543d5e7a95408a6351">
      
  
       
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
        &nbsp;
       <!--
  <div class="views-field views-field-ops">        <span class="field-content"></span>  </div>  
  <div class="views-field views-field-name">        <span class="field-content"><a href="${BASE_PATH}/users/204/tejcis" title="View user profile." class="username" xml:lang="" about="/users/204/tejcis" typeof="sioc:UserAccount" property="foaf:name" datatype="">tejcis</a></span>  </div>  
  <div class="views-field views-field-picture">        <div class="field-content">  <div class="user-picture">
    <a href="${BASE_PATH}/users/204/tejcis" title="View user profile."><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/picture-204-1470869685.jpg" alt="tejcis's picture" title="tejcis's picture"></a>  </div>
</div>  </div>  
  <div class="views-field views-field-php">    <span class="views-label views-label-php">Age</span>    <span class="field-content">21</span>  </div>  
  <div class="views-field views-field-field-hobbies-1">    <span class="views-label views-label-field-hobbies-1">Hobbies</span>    <div class="field-content">Traveling and working abroad &amp; a little bit of everything else. </div>  </div>  
  <div class="views-field views-field-field-bio">    <span class="views-label views-label-field-bio">Short bio</span>    <div class="field-content">Graphic desing student around it's 20s from Slovenia.</div>  </div>  
  <div class="views-field views-field-nid">    <span class="views-label views-label-nid">Blogs</span>    <span class="field-content">4</span>  </div>  
  <div class="views-field views-field-count">    <span class="views-label views-label-count">Followers</span>    <span class="field-content">10</span>  </div>  
   -->
  </div>
    </div>
 
  
  
  
  
  
</div> </div>
<div class="blog_wrapper_main">
	<div class="blog_body">
		<div class="field field-name-body field-type-text-with-summary field-label-hidden">
			<div class="field-items">
				<div class="field-item even" property="content:encoded">
				<p style="text-align:center;font-size:22px">
				${article.title}
				</p>
				<p style="text-align:center;font-size:14px">
				作者：${article.admin.name}&nbsp;&nbsp;&nbsp;&nbsp;发布日期：${article.updateTime?string('yyyy-MM-dd')}
				</p>
				<p>&nbsp;</p>
				<div id="content" name="content">
					<p>&nbsp;</p>
					${article.content}
					
				</div>
				<script type="text/javascript">
					uParse('#content',{rootPath:'../'});
				</script>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	<div class="blog_comments">
		<div class="blog_comment_wrapper">
			<div class="blog_flag_count">
				<div class="blog_flag_count_value">
				1				</div>
				<div class="blog_flaging"></div>
			</div>
			<div class="blog_comment_count">
				<div class="blog_comment_count_value">
				3 
				</div>
			</div>
					
			
 <!--  	
<div id="block-commentsblock-comment-form-block" class="block block-commentsblock">
  <div class="inner"></div>
        
  <div class="content">
    <div id="comments_wrapper" class="comment-wrapper">  

      <a id="comment-1"></a>
<div class="comment clearfix" about="/comment/1#comment-1" typeof="sioc:Post sioct:Comment">
	<div class="comment_left">
        <div class="comment-avatar">
            <div class="user-picture">
    <a href="${BASE_PATH}/users/372/yazid" title="View user profile."><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/picture-372-1471428848.jpg" alt="Yazid's picture" title="Yazid's picture"></a>  </div>
        </div>
		<div class="comment-reply">
            <a class="comment-reply btn btn-sm btn-primary" href="${BASE_PATH}/"> Reply            </a>
        </div>
	</div>
	<div class="comment_right">
        <div class="comment-content">
			<div class="comment-author"><span rel="sioc:has_creator"><a href="${BASE_PATH}/users/372/yazid" title="View user profile." class="username" xml:lang="" about="/users/372/yazid" typeof="sioc:UserAccount" property="foaf:name" datatype="">Yazid</a></span></div>
			<div class="comment_date">17 August 2016</div> 
          
			<div class="content">
				<span rel="sioc:reply_of" resource="/blogs/my-ibiza-experience" class="rdf-meta element-hidden"></span><div class="field field-name-comment-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded">Your message
</div></div></div>			</div>
        </div>
	</div>
 </div>

<a id="comment-2"></a>
<div class="comment clearfix" about="/comment/2#comment-2" typeof="sioc:Post sioct:Comment">
	<div class="comment_left">
        <div class="comment-avatar">
            <div class="user-picture">
    <a href="${BASE_PATH}/users/372/yazid" title="View user profile."><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/picture-372-1471428848.jpg" alt="Yazid's picture" title="Yazid's picture"></a>  </div>
        </div>
		<div class="comment-reply">
            <a class="comment-reply btn btn-sm btn-primary" href="${BASE_PATH}/"> Reply            </a>
        </div>
	</div>
	<div class="comment_right">
        <div class="comment-content">
			<div class="comment-author"><span rel="sioc:has_creator"><a href="${BASE_PATH}/users/372/yazid" title="View user profile." class="username" xml:lang="" about="/users/372/yazid" typeof="sioc:UserAccount" property="foaf:name" datatype="">Yazid</a></span></div>
			<div class="comment_date">17 August 2016</div> 
          
			<div class="content">
				<span rel="sioc:reply_of" resource="/blogs/my-ibiza-experience" class="rdf-meta element-hidden"></span><div class="field field-name-comment-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded">Your message
</div></div></div>			</div>
        </div>
	</div>
 </div>

<a id="comment-3"></a>
<div class="comment clearfix" about="/comment/3#comment-3" typeof="sioc:Post sioct:Comment">
	<div class="comment_left">
        <div class="comment-avatar">
            <div class="user-picture">
    <a href="${BASE_PATH}/users/372/yazid" title="View user profile."><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/picture-372-1471428848.jpg" alt="Yazid's picture" title="Yazid's picture"></a>  </div>
        </div>
		<div class="comment-reply">
            <a class="comment-reply btn btn-sm btn-primary" href="${BASE_PATH}/"> Reply            </a>
        </div>
	</div>
	<div class="comment_right">
        <div class="comment-content">
			<div class="comment-author"><span rel="sioc:has_creator"><a href="${BASE_PATH}/users/372/yazid" title="View user profile." class="username" xml:lang="" about="/users/372/yazid" typeof="sioc:UserAccount" property="foaf:name" datatype="">Yazid</a></span></div>
			<div class="comment_date">17 August 2016</div> 
          
			<div class="content">
				<span rel="sioc:reply_of" resource="/blogs/my-ibiza-experience" class="rdf-meta element-hidden"></span><div class="field field-name-comment-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded">Your message
How are you ?  My name is Liu Mang ,I come from China. 
Could  you send me a message ?</div></div></div>			</div>
        </div>
	</div>
 </div>


       
</div>

    
  </div>
</div>
-->	

		</div>
	</div>
</div>






<!--
<div class="blog_media">
	<div class="details_container">
		<div class="details_title">DETAILS</div>
		<div class="details_detailrow">
			<div class="details_minititle">LOCATION</div>
			<div class="details_detail">
			<div class="field field-name-field-city field-type-taxonomy-term-reference field-label-hidden"><div class="field-items"><div class="field-item even"><a href="${BASE_PATH}/world-cities/ibiza" typeof="skos:Concept" property="rdfs:label skos:prefLabel" datatype="">Ibiza</a></div></div></div>
<div id="block-views-blog-countrie-flag-new-block" class="block block-views">
  <div class="inner"></div>
        
  <div class="content">
    <div class="view view-blog-countrie-flag-new view-id-blog_countrie_flag_new view-display-id-block view-dom-id-507a79f494ba6ae8314c58cb5bfe96cf">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-ops">        <span class="field-content"></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>    
  </div>
</div>

			</div>
		</div>
		<div class="details_detailrow">
			<div class="details_minititle">DURATION</div>
			<div class="details_detail"><div class="field field-name-field-duration field-type-taxonomy-term-reference field-label-hidden"><div class="field-items"><div class="field-item even">2 WEEKS+</div></div></div></div>
		</div>
		<div class="details_seperator"></div>
		<div class="details_detailrow">
			<div class="details_minititle">CATEGORY</div>
			<div class="details_detail"><div class="field field-name-field-category field-type-taxonomy-term-reference field-label-hidden"><div class="field-items"><div class="field-item even"><a href="${BASE_PATH}/category/adventure">ADVENTURE</a>, <a href="${BASE_PATH}/category/nature">NATURE</a>, <a href="${BASE_PATH}/category/party">PARTY</a>, <a href="${BASE_PATH}/category/trip">TRIP</a></div></div></div></div>
		</div>
		<div class="details_detailrow">
			<div class="details_minititle">TAGS</div>
			<div class="details_detail"><div class="field field-name-field-popular-tags field-type-taxonomy-term-reference field-label-hidden"><div class="field-items"><div class="field-item even"><a href="${BASE_PATH}/blog-tags/travel">travel</a>, <a href="${BASE_PATH}/blog-tags/ibiza">ibiza</a>, <a href="${BASE_PATH}/blog-tags/party">PARTY</a>, <a href="${BASE_PATH}/blog-tags/nature">nature</a>, <a href="${BASE_PATH}/blog-tags/exploring">EXPLORING</a>, <a href="${BASE_PATH}/blog-tags/friends">friends</a>, <a href="${BASE_PATH}/blog-tags/family">family</a>, <a href="${BASE_PATH}/blog-tags/job">job</a></div></div></div></div>
		</div>
		<div class="details_seperator"></div>
		<div class="details_detailrow">
			<div class="details_minititle">WHO WENT<br>ALONG</div>
			<div class="details_detail"></div>
		</div>
	</div>
	
<div id="block-simplified-social-share-sharing" class="block block-simplified-social-share">
  <div class="inner"></div>
        
  <div class="content">
    <div class="lrsharecontainer" data-share-url="${BASE_PATH}/blogs/island-vis" data-counter-url="${BASE_PATH}/blogs/island-vis"></div>    
  </div>
</div>

    <div class="view view-blog-bucket-list-add view-id-blog_bucket_list_add view-display-id-entity_view_1 view-dom-id-3bb7fb340b1f9846cd398c63d2087a4c">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-ops">        <span class="field-content"></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div> 	<div class="view view-blog-gallery view-id-blog_gallery view-display-id-entity_view_1 view-dom-id-80cc8b4c7bf8953fa3f6409759f7b35c">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-gallery-upload">    <span class="views-label views-label-field-gallery-upload">GALLERY</span>    <div class="field-content"><a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/13879232_10211058747771143_994836481496731346_n_0.jpg?itok=weoxaKvr" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/13879232_10211058747771143_994836481496731346_n_0.jpg" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/ib1_1.jpg?itok=A1rR_9qU" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/ib1_1.jpg" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4630_0.JPG?itok=w4gJqIfg" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4630_0.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/ib2.jpg?itok=8AZVijSw" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/ib2.jpg" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4758.JPG?itok=ppc1N9Wy" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4758.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4790.JPG?itok=UwUFcdXa" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4790.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4783.JPG?itok=JIVOXFNX" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4783.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4802.JPG?itok=9QoC__WB" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4802.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4814.JPG?itok=LVMC9IbC" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4814.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/ib4_0.jpg?itok=1mk61HR6" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/ib4_0.jpg" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4849_0.JPG?itok=lQ4Q_bkg" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4849_0.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4856_0.JPG?itok=AK4Ft6dt" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4856_0.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_4885_0.JPG?itok=5vCx9aDR" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_4885_0.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/ib5_0.jpg?itok=DYAzDvOt" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/ib5_0.jpg" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/IMG_5171_0.JPG?itok=uNd2a0up" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/IMG_5171_0.JPG" alt="" title=""></a>, <a href="${BASE_PATH}/sites/default/files/styles/large/public/blog_gallery/NSCQ3909_0.jpg?itok=1wAdA-OD" title="My IBIZA experience " class="colorbox init-colorbox-processed cboxElement" rel="gallery-node-231-8khlVXdXom4"><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/NSCQ3909_0.jpg" alt="" title=""></a></div>  </div>  </div>
    </div>
  
  
  
  
  
  
</div> 	<div class="embed_wrapper">
		<div class="textarea_show_small">Embed blog (300x300)</div>
		<div class="textarea_small"><div class="embed_close">x</div>&lt;iframe
 src="http://travel-legendary.bluedreams.si/embed?id=231" width="300" 
height="300" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;</div>
		<div class="textarea_show_big">Embed blog (800x400)</div>
		<div class="textarea_big"><div class="embed_close">x</div>&lt;iframe 
src="http://travel-legendary.bluedreams.si/embed?id=231" width="800" 
height="400" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;</div>
	</div>
    	<div class="blog_comments">
		<div class="blog_comment_wrapper">
			<div class="blog_flag_count">
				<div class="blog_flag_count_value">
				1				</div>
				<div class="blog_flaging"></div>
			</div>
			<div class="blog_comment_count">
				<div class="blog_comment_count_value">
				3 
				</div>
			</div>
			
<div id="block-commentsblock-comment-form-block--2" class="block block-commentsblock">
  <div class="inner"></div>
        
  <div class="content">
    <div id="comments_wrapper" class="comment-wrapper">
  
    
   

      <a id="comment-1"></a>
<div class="comment clearfix" about="/comment/1#comment-1" typeof="sioc:Post sioct:Comment">
	<div class="comment_left">
        <div class="comment-avatar">
            <div class="user-picture">
    <a href="${BASE_PATH}/users/372/yazid" title="View user profile."><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/picture-372-1471428848.jpg" alt="Yazid's picture" title="Yazid's picture"></a>  </div>
        </div>
		<div class="comment-reply">
            <a class="comment-reply btn btn-sm btn-primary" href="${BASE_PATH}/"> Reply            </a>
        </div>
	</div>
	<div class="comment_right">
        <div class="comment-content">
			<div class="comment-author"><span rel="sioc:has_creator"><a href="${BASE_PATH}/users/372/yazid" title="View user profile." class="username" xml:lang="" about="/users/372/yazid" typeof="sioc:UserAccount" property="foaf:name" datatype="">Yazid</a></span></div>
			<div class="comment_date">17 August 2016</div> 
          
			<div class="content">
				<span rel="sioc:reply_of" resource="/blogs/my-ibiza-experience" class="rdf-meta element-hidden"></span><div class="field field-name-comment-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded">Your message
</div></div></div>			</div>
        </div>
	</div>
 </div>

<a id="comment-2"></a>
<div class="comment clearfix" about="/comment/2#comment-2" typeof="sioc:Post sioct:Comment">
	<div class="comment_left">
        <div class="comment-avatar">
            <div class="user-picture">
    <a href="${BASE_PATH}/users/372/yazid" title="View user profile."><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/picture-372-1471428848.jpg" alt="Yazid's picture" title="Yazid's picture"></a>  </div>
        </div>
		<div class="comment-reply">
            <a class="comment-reply btn btn-sm btn-primary" href="${BASE_PATH}/"> Reply            </a>
        </div>
	</div>
	<div class="comment_right">
        <div class="comment-content">
			<div class="comment-author"><span rel="sioc:has_creator"><a href="${BASE_PATH}/users/372/yazid" title="View user profile." class="username" xml:lang="" about="/users/372/yazid" typeof="sioc:UserAccount" property="foaf:name" datatype="">Yazid</a></span></div>
			<div class="comment_date">17 August 2016</div> 
          
			<div class="content">
				<span rel="sioc:reply_of" resource="/blogs/my-ibiza-experience" class="rdf-meta element-hidden"></span><div class="field field-name-comment-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded">Your message
</div></div></div>			</div>
        </div>
	</div>
 </div>

<a id="comment-3"></a>
<div class="comment clearfix" about="/comment/3#comment-3" typeof="sioc:Post sioct:Comment">
	<div class="comment_left">
        <div class="comment-avatar">
            <div class="user-picture">
    <a href="${BASE_PATH}/users/372/yazid" title="View user profile."><img typeof="foaf:Image" src="${TEMPLATE_BASE_PATH}/images/picture-372-1471428848.jpg" alt="Yazid's picture" title="Yazid's picture"></a>  </div>
        </div>
		<div class="comment-reply">
            <a class="comment-reply btn btn-sm btn-primary" href="${BASE_PATH}/"> Reply            </a>
        </div>
	</div>
	<div class="comment_right">
        <div class="comment-content">
			<div class="comment-author"><span rel="sioc:has_creator"><a href="${BASE_PATH}/users/372/yazid" title="View user profile." class="username" xml:lang="" about="/users/372/yazid" typeof="sioc:UserAccount" property="foaf:name" datatype="">Yazid</a></span></div>
			<div class="comment_date">17 August 2016</div> 
          
			<div class="content">
				<span rel="sioc:reply_of" resource="/blogs/my-ibiza-experience" class="rdf-meta element-hidden"></span><div class="field field-name-comment-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded">Your message
How are you ?  My name is Liu Mang ,I come from China. 
Could  you send me a message ?</div></div></div>			</div>
        </div>
	</div>
 </div>


       
</div>

    
  </div>
</div>

		</div>
	</div>
</div>
-->

    
  </div>
</div>

</div>
<!-- END .region-content-->
		</div>
	</div>
</section>
	<#include "../footer.ftl">
</div>	

	
<div id="lightbox2-overlay" style="display: none;"></div>      
<div id="lightbox" style="display: none;" class="lightbox2-orig-layout">        
	<div style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" id="outerImageContainer">
		<div id="modalContainer" style="display: none; padding: 10px;"></div>
		<div id="frameContainer" style="display: none; padding: 10px;"></div>
		<div id="imageContainer" style="display: none; padding: 10px;">
			<img id="lightboxImage" alt="">
			<div id="hoverNav">
				<a style="padding-top: 10px;" id="prevLink" title="Previous" href="#"></a>
				<a style="padding-top: 10px;" id="nextLink" title="Next" href="#"></a>
			</div>
		</div>
		<div id="loading">
			<a href="#" id="loadingLink"></a>
		</div>
	</div>        
	<div style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" id="imageDataContainer" class="clearfix">          
		<div id="imageData">
			<div id="imageDetails">
				<span id="caption"></span>
				<span id="numberDisplay"></span>
			</div>
			<div id="bottomNav">
				<div id="frameHoverNav">
					<a style="padding-top: 10px;" id="framePrevLink" title="Previous" href="#"></a>
					<a style="padding-top: 10px;" id="frameNextLink" title="Next" href="#"></a>
				</div>
				<a style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" id="bottomNavClose" title="Close" href="#"></a>
				<a id="bottomNavZoom" href="#"></a>
				<a id="bottomNavZoomOut" href="#"></a>
				<a id="lightshowPause" title="Pause Slideshow" href="#" style="display: none;"></a>
				<a id="lightshowPlay" title="Play Slideshow" href="#" style="display: none;"></a>
			</div>
		</div>        
	</div>      
</div>
<div style="display: none;" id="cboxOverlay"></div>
<div style="display: none;" tabindex="-1" role="dialog" class="" id="colorbox">
	<div id="cboxWrapper">
		<div>
			<div style="float: left;" id="cboxTopLeft"></div>
			<div style="float: left;" id="cboxTopCenter"></div>
			<div style="float: left;" id="cboxTopRight"></div>
		</div>
		<div style="clear: left;">
			<div style="float: left;" id="cboxMiddleLeft"></div>
			<div style="float: left;" id="cboxContent">
				<div style="float: left;" id="cboxTitle"></div>
				<div style="float: left;" id="cboxCurrent"></div>
				<button id="cboxPrevious" type="button"></button>
				<button id="cboxNext" type="button"></button>
				<button id="cboxSlideshow"></button>
				<div style="float: left;" id="cboxLoadingOverlay"></div>
				<div style="float: left;" id="cboxLoadingGraphic"></div>
			</div>
			<div style="float: left;" id="cboxMiddleRight"></div>
		</div>
		<div style="clear: left;">
			<div style="float: left;" id="cboxBottomLeft"></div>
			<div style="float: left;" id="cboxBottomCenter"></div>
			<div style="float: left;" id="cboxBottomRight"></div>
		</div>
	</div>
	<div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div>
</div>
</body>
</html>