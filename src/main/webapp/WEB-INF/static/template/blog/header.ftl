<!DOCTYPE html>
<html class="js">
<head>

<title>${shamrock_seo_title}</title>
<!-- 
<link rel="shortcut icon" href="${TEMPLATE_BASE_PATH}/images/favicon.ico" type="image/vnd.microsoft.icon">
 -->
<link type="text/css" rel="stylesheet" href="${TEMPLATE_BASE_PATH}/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all">
<link type="text/css" rel="stylesheet" href="${TEMPLATE_BASE_PATH}/css/css_UZ65h848a8E2HYNdjv61G9Rbt-qWK-KwNB09phU7z7s.css" media="all">
<link type="text/css" rel="stylesheet" href="${TEMPLATE_BASE_PATH}/css/css_6LrQVNKucdk3oYY7Nn5rfVQr9fEeKdUEDRSuu9kCy5I.css" media="all">
<link type="text/css" rel="stylesheet" href="${TEMPLATE_BASE_PATH}/css/css_47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU.css" media="screen and (min-width: 992px)">
<link type="text/css" rel="stylesheet" href="${TEMPLATE_BASE_PATH}/css/css_4BBwRTjUzEAy4fpqbbheWfB-gci4_VOD8CGJBcHLwqQ.css" media="screen and (max-width: 991px)">
<link type="text/css" rel="stylesheet" href="${TEMPLATE_BASE_PATH}/css/css_C39wsmZHp9reoRQlZLKZjDOSNJIZQ_jif_MVimgu1Rg.css" media="all">
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/js_seGfplOEmoqT0scKieAF4Tg5m-aZYFbmYFRZ5FHSP6g.js"></script>
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/js_ixx6ICBRi9DlwuTxMIdlLu-CCDJHqz4cBzTFOp0n6fg.js"></script>
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/js_4baF1JxYZlP3X8kt4-tIMBfhHeww76-WTJMjFxCOrmI.js"></script>
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/js_8QmrplhpAibOYgGZQWgdTMJVQqLSgsY6zl9hMHiAINk.js"></script>
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/lightbox.js"></script>
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/js_8O3K0EF7pOil7qj9kuDBzMynNQLSk-_4DqrwhQ3Taao.js"></script>
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/js_RBre59epjE5tJaCI9ZZvU_8UhsnMOcfUQlcvSgK4voU.js"></script>
<script type="text/javascript" src="${TEMPLATE_BASE_PATH}/js/js_47gsHHKUOoENkSAgKv5bCHHLcAt0pE_LH_EDyhEdIhs.js"></script>
<link href="${TEMPLATE_MANAGE_PATH}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/assets/ueditor/ueditor.parse.js"> </script>
<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/js/jquery.js"> </script>
   	<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/assets/ueditor/ueditor.all.min.js"> </script>
   	<script type="text/javascript" charset="utf-8" src="${TEMPLATE_MANAGE_PATH}/assets/ueditor/lang/zh-cn/zh-cn.js"></script>
	<!--common script for all pages-->
	<script src="${TEMPLATE_MANAGE_PATH}/js/common-scripts.js"></script>
</head>
<body class="html not-front not-logged-in no-sidebars page-blogs ltr wide  preset-white-blue lightbox-processed dexp-lg">

		<div id="skip-link">
		<a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
		</div>
		<div class="dexp-body-inner default">
		<section class="dexp-section" id="section-top">
			<div class="container">
				<div class="row">
					<!-- .region-social-top-->
  					<div class="region region-social-top col-xs-12 col-sm-12 col-md-7 col-lg-7">
						<div id="block-block-46" class="block block-block hidden-xs hidden-sm">
  							<div class="inner"></div>
  							<div class="content">
   								 <div class="social clearfix">
		
								</div>
    
  							</div>
						</div>

  					</div>
					<!-- END .region-social-top-->
					<!-- .region-contact-info-->
 					 <div class="region region-contact-info col-xs-12 col-sm-8 col-md-5 col-lg-5">
						<div id="block-tl-general-user-links-tl-general" class="block block-tl-general">
						  <div class="inner"></div>
						        
						  <div class="content">
						  <#if SESSION_ADMIN?exists>
						    <a href="${BASE_PATH}/manage/article/list.htm"><i class="icon-user"></i>欢迎，${SESSION_ADMIN.name}</a>&nbsp;&nbsp; &nbsp; &nbsp; <a href="${BASE_PATH}/admin/logout.htm"><i class="icon-signout"></i>退出</a>    
						  <#else>
						    <a href="${BASE_PATH}/admin/login.htm">登录</a>&nbsp;&nbsp; &nbsp; &nbsp; <a href="${BASE_PATH}/admin/register.htm">注册</a>    
						  </#if>
						  </div>
						</div>
  					</div>
				 <!-- END .region-contact-info-->
			</div>
		</div>
	</section>
	<div style="height: 101px;" class="sticky-wrapper">
	<section style="width: 1903px;" class="dexp-section dexp-sticky" id="section-header">
		<div class="container">
			<div class="row">
				<!-- .region-logo-->
				<div class="col-xs-8 col-sm-10 col-md-2 col-lg-2"><a href="" class="site-logo"><#if LOGO??><img src="${BASE_PATH}/${LOGO}" alt=""></#if></a></div>
				<!-- END .region-logo-->
				<!-- .region-navigation-->
	 			 <div class="region region-navigation col-xs-2 col-sm-1 col-md-10 col-lg-10">
					<div id="block-dexp-menu-dexp-menu-block-1" class="block block-dexp-menu">
		  				<div class="inner"></div>
		  				<div class="content">
		  					  <a data-target="#dexp-dropdown" href="#" class="hidden-lg hidden-md btn btn-inverse dexp-menu-toggler">
							  <i class="fa fa-bars"></i>
							 </a>
							<div id="dexp-dropdown" class="dexp-menu dexp-dropdown">
								<ul class="menu">
								<@shamrock_folder_list_tag folderId=WEBID>
									<li class="first leaf"><a href="${BASE_PATH}/" <#if g_folderId==0>class="active"</#if>>首页</a></li>
								 <#list tag_folder_list as tag_folder>
								 	<li class="<#if tag_folder_index==0>first leaf<#elseif tag_folder_has_next>leaf<#else>last leaf</#if>"><a href="<@shamrock_folder_url_tag ename=tag_folder.ename/>" <#if tag_folder.folderId==g_folderId>class="active"</#if>>${tag_folder.name}</a></li>
								 </#list>
								 <!--
								<li class="first leaf"><a href="http://www.travelegendary.com/blogs" class="active">Blog Library</a></li>
								<li class="leaf"><a href="http://www.travelegendary.com/users">Users</a></li>
								<li class="last leaf"><a href="http://www.travelegendary.com/story-behind">ABOUT US</a></li>
								-->
								</@shamrock_folder_list_tag>
								</ul>
							</div>    
		  				</div>
					</div>
	  			</div>
			  <!-- END .region-navigation-->
			</div>
		</div>
	</section>
	</div>
	
	
	<section class="dexp-section" id="section-page-title">
		<div class="container">
			<div class="row">
				<!-- .region-pagetitle-->
	 			 <div class="region region-pagetitle col-xs-12 col-sm-12 col-md-7 col-lg-7">
					<div id="block-dexp-page-elements-dexp-page-elements-page-title" class="block block-dexp-page-elements">
		 			    <div class="inner"></div>
						<div class="content">
		    				<div class="page_title_no_sub"><h1 class="page_title">无论什么时候，都别忘了自己将要去的地方</h1></div>    
		  				</div>
					</div>
	 			</div>
			   <!-- END .region-pagetitle-->
			</div>
		</div>
	</section>