			<!-- .region-left-sidebar-->
 			<div class="region region-left-sidebar col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<div id="block-views-exp-blogs-page" class="block block-views">
 				<div class="inner"></div>
  				<div class="content">
    				<form class="ctools-auto-submit-full-form jquery-once-2-processed ctools-auto-submit-processed" action="${BASE_PATH}/search.htm" method="get" id="views-exposed-form-blogs-page" accept-charset="UTF-8">
    					<div>
    						<div class="views-exposed-form">
  								<div class="views-exposed-widgets clearfix">
          							<div id="edit-tid-wrapper" class="views-exposed-widget views-widget-filter-tid">
                  						<label for="edit-tid">搜索</label>
                        				<div class="views-widget">
          									<div role="application" class="form-item form-type-textfield form-item-tid">
 												<input aria-autocomplete="list" autocomplete="OFF" id="edit-tid" name="tid" size="60" maxlength="128" class="form-text form-autocomplete ctools-auto-submit-processed" type="text">
 												<input id="edit-tid-autocomplete" value="" disabled="disabled" class="autocomplete autocomplete-processed" type="hidden">
												<span id="edit-tid-autocomplete-aria-live" class="element-invisible" aria-live="assertive"></span>
											</div>
        								</div>
              						</div>
          							<div id="edit-field-duration-tid-wrapper" class="views-exposed-widget views-widget-filter-field_duration_tid">
                  						<label for="edit-field-duration-tid">按日期检索</label>
                        				<div class="views-widget">
          									<div class="form-item form-type-select form-item-field-duration-tid">
 												<div class="form-checkboxes bef-select-as-checkboxes bef-required-filter-processed">
 													<div class="bef-checkboxes">
 														<div class="form-item form-type-bef-checkbox form-item-edit-field-duration-tid-19">
 															<input name="inputtime" id="edit-field-duration-tid-19" value="7" type="radio"> 
 															<label class="option" for="edit-field-duration-tid-19">一周内</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-duration-tid-20">
 															<input name="inputtime" id="edit-field-duration-tid-20" value="30" type="radio"> 
 															<label class="option" for="edit-field-duration-tid-20">一月内</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-duration-tid-17">
 															<input name="inputtime" id="edit-field-duration-tid-17" value="180" type="radio"> 
 															<label class="option" for="edit-field-duration-tid-17">半年内</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-duration-tid-18">
															 <input name="inputtime" id="edit-field-duration-tid-18" value="365" type="radio"> 
															 <label class="option" for="edit-field-duration-tid-18">一年内</label>
														</div>
													</div>
												</div>
											</div>
       									 </div>
              						</div>
              						<!-- 
          							<div id="edit-field-category-tid-wrapper" class="views-exposed-widget views-widget-filter-field_category_tid">
                  						<label for="edit-field-category-tid">栏目</label>
                        				<div class="views-widget">
          									<div class="form-item form-type-select form-item-field-category-tid">
 												<div class="form-checkboxes bef-select-as-checkboxes bef-required-filter-processed">
 													<div class="bef-checkboxes">
 														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-32285">
															 <input name="field_category_tid[]" id="edit-field-category-tid-32285" value="32285" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-32285">ADVENTURE</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-32281">
															 <input name="field_category_tid[]" id="edit-field-category-tid-32281" value="32281" type="checkbox">
															 <label class="option" for="edit-field-category-tid-32281">ART</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-21">
								 							<input name="field_category_tid[]" id="edit-field-category-tid-21" value="21" type="checkbox"> 
								 							<label class="option" for="edit-field-category-tid-21">CUISINE</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-32283">
								 							<input name="field_category_tid[]" id="edit-field-category-tid-32283" value="32283" type="checkbox"> 
								 							<label class="option" for="edit-field-category-tid-32283">FAMILY</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-32282">
															 <input name="field_category_tid[]" id="edit-field-category-tid-32282" value="32282" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-32282">HISTORY</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-25">
															 <input name="field_category_tid[]" id="edit-field-category-tid-25" value="25" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-25">LOW BUDGET</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-32286">
															 <input name="field_category_tid[]" id="edit-field-category-tid-32286" value="32286" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-32286">LUXURY</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-26">
															 <input name="field_category_tid[]" id="edit-field-category-tid-26" value="26" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-26">NATURE</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-23">
															 <input name="field_category_tid[]" id="edit-field-category-tid-23" value="23" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-23">PARTY</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-32284">
															 <input name="field_category_tid[]" id="edit-field-category-tid-32284" value="32284" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-32284">ROMANCE</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-22">
															 <input name="field_category_tid[]" id="edit-field-category-tid-22" value="22" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-22">SPORTS</label>
														</div>
														<div class="form-item form-type-bef-checkbox form-item-edit-field-category-tid-28">
															 <input name="field_category_tid[]" id="edit-field-category-tid-28" value="28" type="checkbox"> 
															 <label class="option" for="edit-field-category-tid-28">TRIP</label>
														</div>
													</div>
												</div>
											</div>
        								</div>
              						</div>
              						
              						
          							<div id="edit-field-popular-tags-tid-wrapper" class="views-exposed-widget views-widget-filter-field_popular_tags_tid">
                  						<label for="edit-field-popular-tags-tid">Search tags          </label>
                       					<div class="views-widget">
          									<div role="application" class="form-item form-type-textfield form-item-field-popular-tags-tid">
												 <input aria-autocomplete="list" autocomplete="OFF" id="edit-field-popular-tags-tid" name="field_popular_tags_tid" size="60" maxlength="128" class="form-text form-autocomplete ctools-auto-submit-processed" type="text">
												 <input id="edit-field-popular-tags-tid-autocomplete" value="http://www.travelegendary.com/?q=admin/views/ajax/autocomplete/taxonomy/1" disabled="disabled" class="autocomplete autocomplete-processed" type="hidden">
												 <span id="edit-field-popular-tags-tid-autocomplete-aria-live" class="element-invisible" aria-live="assertive"></span>
											</div>
        								</div>
              						</div>
                    				<div class="views-exposed-widget views-submit-button">
     									 <input class="ctools-use-ajax ctools-auto-submit-click js-hide btn btn-primary form-submit" id="edit-submit-blogs" name="" value="Apply" type="submit">    
     								</div>
     								-->
	      						</div>
							</div>
						</div>
					</form>    
	  			</div>
			</div>
			<div id="block-tagclouds-1" class="block block-tagclouds">
	  			<div class="inner"></div>
	          	<h2 class="block-title  no-subtitle">栏目</h2>
	  			<div class="content">
	  			<#if g_folderId==0>
					<@shamrock_folder_list_tag folderId=WEBID>
						<#list tag_folder_list as tag_folder>
						   <span class="tagclouds-term"><a href="<@shamrock_folder_url_tag ename=tag_folder.ename/>" class="tagclouds level6" title="">${tag_folder.name}</a></span>
						 </#list>
					 </@shamrock_folder_list_tag>
				<#else>
					<#list folderList as tag_folder>
						   <span class="tagclouds-term"><a href="<@shamrock_folder_url_tag ename=tag_folder.ename/>" class="tagclouds level6" title="">${tag_folder.name}</a></span>
					</#list>
	  			</#if>
	  			</div>
			</div>
	  	</div>
		<!-- END .region-left-sidebar-->